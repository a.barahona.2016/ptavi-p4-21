#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys



def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        SERVER = sys.argv[1]
        PORT = int(sys.argv[2])
        LINE = sys.argv[3]
        USERNAME = sys.argv[4]
        EXPIRES = sys.argv[5]
    except ValueError:
        sys.exit("puerto and ip must be numbers")
    except IndexError:
        sys.exit("Usage: client.py <ip> <puerto> register <sip_address> <expires_value>")

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            print(LINE.upper() + ' sip:' + USERNAME + ' SIP/2.0 ')
            print("Expires: " + str(EXPIRES))
            print()
            my_socket.send(bytes(LINE.upper() + ' sip:' + USERNAME + ' SIP/2.0 ' + EXPIRES + '\r\n', 'utf-8') + b'\r\n\r\n')
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
