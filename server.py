#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time

# Constantes. Puerto.
PORT = int(sys.argv[1])


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}
    lista_users = []
    lista_servers = []

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        for line in self.rfile:
            if line != b'\r\n':
                print(str(self.client_address[0]) + " " + str(self.client_address[1]) + " " + line.decode('utf-8'))
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                lista = line.decode('utf-8').split(" ")
                print(lista)
                palabra = lista[3]
                expires = palabra[:4]
                tiempo = time.strftime("%Y-%m-%d %H:%M:%S",
                                       time.gmtime(int(time.time()) + int(expires)))
                username = lista[1]
                self.lista_users.append(username[4:])
                self.lista_servers.append(str(self.client_address[0]))
                self.dicc["username"] = self.lista_users
                self.dicc["server"] = str(self.lista_servers)
                self.dicc["expires"] = tiempo

        self.register2json()
    def register2json(self):
        with open('registered.json', 'w') as jsonfile:
            json.dump(self.dicc, jsonfile, indent=4)


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

